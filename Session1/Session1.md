[<- Table of Contents](https://gitlab.com/involta-bis/demos/containers/-/blob/main/README.md)

# Intro to Containers

## Key Links
- [Slides](https://gitlab.com/i3737/demos-and-training/containers/-/blob/main/Session1/Container%20Overview.pdf)

## Introduction
- Containers vs VMs
- Docker
  - Container runtime
  - Packaging engine
  - CLI
- Key concepts
  - Images
  - Repositories
  - Containers
## Demos
### Launching your first container
> Note: the ```-it``` argument on docker run causes the container to run in interactive mode, leveraging the tty to allow shell access
1. Ubuntu bash access
```docker
# run a container with the latest Ubuntu image
docker run --name myUbuntu -it ubuntu /bin/bash

# Show the running container
docker ps -af "name=myUbuntu"

# see resource consumption for running containers
docker stats --no-stream

# List all images installed locally
docker image ls

# Clean up the test 'myUbuntu' container
docker stop myUbuntu
docker rm myUbuntu
```

2. Run *nginx* web server
```docker
# start container in daemon mode, listening on host port 12345
docker run --name vanillaNginx -p 12345:80 -d nginx
# access web server at http://localhost:12345/

# note container is running
docker ps -f "name=vanillaNginx"

# Stop and start the container.  Notice quick response times
docker stop vanillaNginx
docker start vanillaNginx

# check resource utilization
docker stats --no-stream
```

3. Run *nginx* web server with customizations
```docker
# start container in daemon mode, listening on host port 12345
docker run --name customNginx -p 12346:80 -v (pwd)/nginxRoot/:/usr/share/nginx/html:ro -d nginx
# access web server at http://localhost:12346/

# note container is running
docker ps -f "name=customNginx"

# check resource utilization
docker stats --no-stream
```

4. Create new image that includes the custom web content
    1. Create a docker file as follows
    ```docker
    FROM nginx
    COPY ./nginxRoot /usr/share/nginx/html
    ```
    2. Build the image and run it
    ```docker
    docker build -t mynginx:latest -f Dockerfile .
    docker run --name nginxImage -p 12347:80 -d mynginx
    ```

5. Teardown and cleanup
```docker
# Stop all running containers
docker stop nginxImage
docker stop customNginx
docker stop vanillaNginx

# Remove the containers from disk
docker rm nginxImage
docker rm customNginx
docker rm vanillaNginx

# Remove container images from the local store
docker image rm nginx
docker image rm ubuntu
docker image rm nginximage
```

