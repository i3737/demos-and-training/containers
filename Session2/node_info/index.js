const http = require('node:http')
const terminator = require('http-terminator')
const fs = require('fs')
const { stringify } = require('node:querystring')

const get_env_info = () => {
  let r = []
  let e = process.env
  for (key in e) {
    if (key.startsWith('KUBE_')) {
      r.push({ key: key, value: e[key] })
    }
  }
  return r
}
// Create a local server to receive data from
const server = http.createServer((req, res) => {
  console.log(req.url)
  let header = {'Content-Type': 'text/html'}
  let status = 200
  let s
  let body = ''
  switch (req.url) {
    case '/':
      body = fs.readFileSync('./index.html')
      break;
    case '/data':
      header = {'Content-Type': 'application/json'}
      body = JSON.stringify(get_env_info())
      break
    case '/logo':
      let file = 'involta_logo.png'
      header = { 'Content-Type': 'image/png' }
      s = fs.createReadStream(file)
      break
    case '/kill.png':
      header = { 'Content-Type': 'image/png' }
      s = fs.createReadStream('kill.png')
      break
    case '/kill':
      httpTerminator.terminate()
      body = 'bye bye' 
      break
    default:
      status = 404
      body = 'not found'
    }
    res.writeHead(status, header);
    if (s)
      s.on('open', function () {
          s.pipe(res);
      })
    else
      res.end(body);

    
});

const httpTerminator = terminator.createHttpTerminator({ server })


server.listen(8021);