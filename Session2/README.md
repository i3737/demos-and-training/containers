[<- Table of Contents](https://gitlab.com/involta-bis/demos/containers/-/blob/main/README.md)

# Intro to Containers
## Session 2: Intro to Kubernetes and Helm
### Kubernetes
Kubernetes is a service originally devloped by Google, open-sources, and now managed by the [Cloud Native Computing Foundation](https://www.cncf.io/)
- derived from their Borg system, intended to simplify Borg
- built to scale to thousands of nodes
- enables scalability and rubustness of workloads
- fully dependent upon container technology
- wide adoption across industry
  - large ecosystem of supporting tools / capabilities
  - numerous distributions (opinionation)

To understand Kubernets, it is important to understand some key terms / concepts
| Object Type | Description |
|-|-|
| Cluster | A set of physical nodes which work together to serve workloads in a scalable, robust manner |
| Nodes | Logical representation of physical machines which comprise the cluster.  Nodes may fulfill various roles, including 'master', 'control plane', and 'worker' |
| Manifests | YAML files which contain definitions / configurations for Kubernetes |
| Deployments | A set of other logical constructs run on a Kubernetes cluster which comprise a hosted application or workload |
| Pods | One or more containers working together to perform a function.  A good analogy is a microservice |
| Services | A network routing construct which distributes traffic into / between pods in a cluster |
| Ingress | A networking construct which bridges external networks to internal cluster virtual networks |
| Others | There are numerous other resource types recognized by CNCF, as well as a litany of third party object types to enhance functionality of the platform (e.g. DaemonSets, ScaleSets, PersistenVolumes, Istio, Portworx, MetalLB)  However, these other types are outside the scope of this session |
---

### Helm Charts
[Helm](https://helm.sh) is a package management technology for building, deploying, and managing Kubernetes manifest files.  One key feature of Helm is the ability to parameterize deployments, enabling reuse of charts in a variety of configuration scenarios.
---

## Key Links
- N/A
---

## Demo Deployment Structure
![Architecture Diagram](architecture.png)
*Deployment Architecture*

## Prerequisites
1. Install K3s on multiple nodes
```bash
# Generate a secret
openssl rand -base64 32

# First master node
curl -sfL https://get.k3s.io | sh -s - server --token=<secret> --tls-san k3.lan --node-name <name> --cluster-init

# Additional master nodes
curl -sfL https://get.k3s.io | sh -s - server --token=<secret> --tls-san k3.lan --node-name <name> --server https://<server1>:6443

# Add agent nodes
curl -sfL https://get.k3s.io | K3S_URL=https://k3.lan:6443 K3S_TOKEN=involta K3S_NODE_NAME=member1 sh -
```
> Note: this configuration introduces an issue in intra-cluster service communication.  Troubleshooting is underway.
--- 
 
## kubectl - Inspect the cluster
1. Get cluster info
```bash
kubectl cluster-info
```

2. View cluster nodes
```bash
kubectl get nodes
```
---

## Build and publish app image
1. Build the image
```bash
docker build -t involta/node-info:latest .
```

2. Publish to [hub.docker.com](https://hub.docker.com/repository/docker/involta/node-info)
```bash
docker push involta/get-info:latest
```
---

## Helm Deployment
1. Update or install helm chart.  By default, this chart will create a single pod replica.
```bash
# install or update
cd <chart directory>
helm upgrade --install nodeinfo .

# set replica count to desired value
kubectl -n node-info scale --replicas=6 deployment/nodeinfo-node-info

# view pod status
kubectl -n node-info get pods

# view service
kubectl -n node-info get svc

# view ingress
kubectl -n node-info get ingress
```
---

## App Testing
1. Monitor pods
```bash
kubectl -n node-info get pods -o wide -w
```
2. Access application [http://<hostname or IP>](http://node-info/)
3. Refresh to access multiple pods.  Note the pod name changes as the service distributes requests to available pods
4. Click the [kill button](http://node-info/kill) to terminate the web server in a connected pod
5. Note the pod restarts automatically

